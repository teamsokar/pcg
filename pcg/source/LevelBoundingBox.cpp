#include "..\pch.h"
#include "..\include\pcg\LevelBoundingBox.h"

skr::pcg::LevelBoundingBox::LevelBoundingBox()
    : _center(Point3d{0.0f, 0.0f, 0.0f}),
      _LLB(Point3d{0.0f, 0.0f, 0.0f}),
      _LRB(Point3d{0.0f, 0.0f, 0.0f}),
      _ULB(Point3d{0.0f, 0.0f, 0.0f}),
      _URB(Point3d{0.0f, 0.0f, 0.0f})     
{
}

skr::pcg::LevelBoundingBox::LevelBoundingBox(Point3d center, float width, float length)
    : _center(center)
{
    _LLB  = Point3d {_center[0] - width * 0.5f, 0.0f, _center[2] - length * 0.5f};
    _URB = Point3d {_center[0] + width * 0.5f, 0.0f, _center[2] + length * 0.5f};

    _LRB = Point3d {_LLB[0], 0.0f, _URB[2]};
    _ULB  = Point3d {_URB[0], 0.0f, _LLB[2]};
}

skr::pcg::LevelBoundingBox::LevelBoundingBox(skr::pcg::Room room)
    : _center(Point3d{room._center[0], room._center[1], room._center[2]})
{
    _LLB  = Point3d {_center[0] - room._width * 0.5f, 0.0f, _center[2] - room._length * 0.5f};
    _URB = Point3d {_center[0] + room._width * 0.5f, 0.0f, _center[2] + room._length * 0.5f};
                  
    _LRB = Point3d {_LLB[0], 0.0f, _URB[2]};
    _ULB  = Point3d {_URB[0], 0.0f, _LLB[2]};
}

skr::pcg::LevelBoundingBox::LevelBoundingBox(std::vector<skr::pcg::Room> rooms)
{
    _LLB  = Point3d { std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max() };
    _URB = Point3d { std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min() };

    for (auto r : rooms)
    {
        if (r._center[0] - r._width * 0.5f < _LLB[0])
            _LLB[0] = r._center[0] - r._width * 0.5f;

        _LLB[1] = 0.0f; // currently, everything is on one plane, so this is enough

        if (r._center[2] - r._length * 0.5f < _LLB[2])
            _LLB[2] = r._center[2] - r._length * 0.5f;


        if (r._center[0] + r._width * 0.5f > _URB[0])
            _URB[0] = r._center[0] + r._width * 0.5f;

        _URB[1] = 0.0f; // currently, everything is on one plane, so this is enough

        if (r._center[2] + r._length * 0.5f > _URB[2])
            _URB[2] = r._center[2] + r._length * 0.5f;
    }

    _LRB = Point3d {_URB[0], 0.0f, _LLB[2]};
    _ULB  = Point3d {_LLB[0], 0.0f, _URB[2]};

    auto w = _URB[0] - _LLB[0];
    auto h = _URB[2] - _LLB[2];

    _center = Point3d{_LLB[0] + w * 0.5f, 0.0f, _LLB[2] + h * 0.5f};
}
