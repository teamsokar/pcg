#include <iostream>

#include <vector>
#include <string>
#include <ctime>

#pragma warning(push, 0)
#include "CImg.h"
#pragma warning(pop)

#include "pcg/Definitions.h"

#include "pcg/Room.h"
#include "pcg/Target.h"
#include "pcg/LevelBoundingBox.h"
#include "pcg/FloorGenerator.h"

using namespace cimg_library;
using namespace skr::pcg;

std::string GetCurrentDateAndTime();
std::vector<Room> GenerateRooms(int noRooms);
LevelBoundingBox GenerateBoundingBox(std::vector<Room> rooms);
void SaveRoomsAsImageFile(CImg<unsigned char> image);
std::vector<Room> TranslateIntoViewSpace(std::vector<Room> rooms, LevelBoundingBox bbox);

//! \brief Visual demo application for pcg lib
//! draws rooms and targets using the Cimg library
int main()
{
    uint8_t noRooms(10);

    auto rawrooms = GenerateRooms(noRooms);
    LevelBoundingBox bbox = GenerateBoundingBox(rawrooms);

    std::cout << "================================================================================" << std::endl;

    auto rooms = TranslateIntoViewSpace(rawrooms, bbox);

    // Defaults: Room sizes: 1-10, Coordinates 0-100
    CImg<unsigned char> image(100, 100, 1, 3, 0);
    CImgDisplay main_disp(image, "Rooms");

    // define colors in a way they can be iterated
    // TODO those ugly static_casts...
    std::vector<std::array<char, 3>> colors =
    {
        { static_cast<char>(255), 0, 0 }, // red
        { 0, static_cast<char>(255), 0 }, // blue
        { 0, 0, static_cast<char>(255) }, // green
        { static_cast<char>(255), 0, static_cast<char>(255) }, // yellow
        { static_cast<char>(255), static_cast<char>(255), 0 }, // purple
        { 0, static_cast<char>(255), static_cast<char>(255) }, // turquoise
        { static_cast<char>(128), static_cast<char>(128), static_cast<char>(128) }, // grey
        { static_cast<char>(128), static_cast<char>(255), static_cast<char>(255) }, // light turquoise
        { static_cast<char>(255), static_cast<char>(128), static_cast<char>(255) }, // light pink
        { static_cast<char>(255), static_cast<char>(255), static_cast<char>(128) }  // light yellow
    };

    char red[3] = { static_cast<char>(255), 0, 0 };
    char white[3] = { static_cast<char>(255), static_cast<char>(255), static_cast<char>(255) };
    char green[3] = { 0, static_cast<char>(255), 0 };
    char blue[3] = { 0, 0, static_cast<char>(255)};

    bool drawBBox(false);
    
    // "Render Loop"
    while (!main_disp.is_closed() && !main_disp.is_keyESC())
    {
        main_disp.display(image);

        // draw rooms
        for (auto& r : rooms)
        {
            auto geom = r.GenerateGeometry3D();
            auto face = geom[0]; // only draw top face, its enough for this 2d visualization
            //for (auto& face : geom)
            {
                char color[3]{ static_cast<char>(face.color[0] * 255), static_cast<char>(face.color[1] * 255) , static_cast<char>(face.color[1] * 255) };

                image.draw_rectangle
                (
                    r._center[0] - r._width * 0.5f,
                    r._center[2] + r._length * 0.5f,
                    r._center[0] + r._width * 0.5f,
                    r._center[2] - r._length * 0.5f,
                    color,
                    0.5f
                );
            }

            for (auto t : r._targets)
            {
                image.draw_rectangle
                (
                    t._position[0] - t._width * 0.5f,
                    t._position[2] + t._length * 0.5f,
                    t._position[0] + t._width * 0.5f,
                    t._position[2] - t._length * 0.5f,
                    green,
                    0.3f
                );
            }
        }

        if (drawBBox)
        {
            // draw bounding box
            image.draw_point(bbox._center[0], bbox._center[2], white, 1.0f);

            image.draw_line(bbox._ULB[0], bbox._ULB[2], bbox._URB[0], bbox._URB[2], white);
            image.draw_line(bbox._URB[0], bbox._URB[2], bbox._LRB[0], bbox._LRB[2], white);
            image.draw_line(bbox._LRB[0], bbox._LRB[2], bbox._LLB[0], bbox._LLB[2], white);
            image.draw_line(bbox._LLB[0], bbox._LLB[2], bbox._ULB[0], bbox._ULB[2], white);
        }

        // handy debug options
        if(main_disp.is_keyF7())
            image.display();

        if (main_disp.is_keyF6())
        {
            drawBBox = !drawBBox;
            image.assign(200, 200, 1, 3, 0);
        }

        if (main_disp.is_keyF5())
        {
            rooms.clear();
            rawrooms.clear();

            rawrooms = GenerateRooms(noRooms);
            bbox = GenerateBoundingBox(rawrooms);
            rooms = TranslateIntoViewSpace(rawrooms, bbox);

            image.assign(200, 200, 1, 3, 0);
        }

        if (main_disp.is_keyF4())
        {
            SaveRoomsAsImageFile(image);
        }
    }

    SaveRoomsAsImageFile(image);
}

std::string GetCurrentDateAndTime()
{
    std::time_t t = std::time(nullptr);
    char mbstr[20];
#pragma warning (disable:4996)
    if (std::strftime(mbstr, sizeof(mbstr), "%Y%m%d_%H%M%S", std::localtime(&t)))
#pragma warning(default: 4996)
    {
        return std::string(mbstr);
    }
    else
    {
        std::cerr << "could not convert time to readable format" << std::endl;
    }

    return std::string();
}

std::vector<Room> GenerateRooms(int noRooms)
{
    FloorGeneratorParameter params(noRooms, FloorLayoutStrategy::Stacking);
    params._rng_randomSeed = false;
    params._rng_seed = 1689098912;    
    params._rng_printSeed = true;
    FloorGenerator generator(params);

    std::cout << "Rooms" << "\n";
    std::cout << "Number of Rooms to generate: " << noRooms << "\n";
    
    int noRs = 0, noHWs = 0;
    for (auto& r : generator.GetAllGeometry())
    {
        switch (r._roomType)
        {
        case RoomType::Room:
            noRs++;
            break;

        case RoomType::Hallway:
            noHWs++;
            break;
        case RoomType::Start:
            break;
        case RoomType::Finish:
            break;
        default:
            std::cout << "####ERROR: unknow type of room found";
        }
    }

    std::cout << "Number of Rooms generated: " << noRs << " Number of Hallways generated: " << noHWs << "\n";
    
    if (noRs != noRooms)
        std::cout << "##### ERROR: number of rooms generated does not match supposed number" << "\n";

    if (noHWs != noRooms - 1)
        std::cout << "##### ERROR: number of hallways generated does not match supposed number" << "\n";

    for (auto& r : generator.GetAllGeometry())
    {
        std::cout << r.PrintRoomMeasurements() << "\n";
    }

    std::cout << std::endl;

    return(generator.GetAllGeometry());
}

LevelBoundingBox GenerateBoundingBox(std::vector<Room> rooms)
{
    LevelBoundingBox bbox(rooms);
    std::cout << "Bounding Box: " << "Cx: " << bbox._center[0] << " Cy: " << bbox._center[1] << " Cz: " << bbox._center[2] << std::endl;
    return bbox;
}

void SaveRoomsAsImageFile(CImg<unsigned char> image)
{
    // save BMP-File
    std::string fname = GetCurrentDateAndTime() + ".bmp";
    image.save_bmp(fname.c_str());
}

// translate from OpenGL-compatibel coordiante system to ViewCoordinateSystem for cimage lib
// cimage has 0,0 at top left, with no negative values in view space by default
// use upperleft coordinate of bounding box to move everything to lower right quadrant, then flip on x-axis (in 2D-space)
std::vector<Room> TranslateIntoViewSpace(std::vector<Room> rooms, LevelBoundingBox bbox)
{
    float offsetX (bbox._ULB[0] > 0.0f ? bbox._ULB[0] : -bbox._ULB[0]);
    float offsetY (bbox._ULB[1] > 0.0f ? bbox._ULB[1] : -bbox._ULB[1]);
    float offsetZ (bbox._ULB[2] > 0.0f ? bbox._ULB[2] : -bbox._ULB[2]);

    std::vector<Room> translated_rooms;
    translated_rooms.reserve(rooms.size());

    for (const auto& r : rooms)
    {
        Room rn(r);

        rn._center = Vec3 { r._center[0] + offsetX, 0.0f, -(r._center[2] - offsetZ)};
        
        for (auto& t : rn._targets)
        {
            t._position[0] = t._position[0] + offsetX;
            t._position[1] = 0.0f;
            t._position[2] = -(t._position[2] - offsetZ);

            t._width = t._width;
        }

        translated_rooms.push_back(rn);
    }

    return translated_rooms;
}