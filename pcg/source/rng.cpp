#include "..\pch.h"
#include "..\include\pcg\rng.h"

#include <iostream>

#include <chrono>

skr::pcg::RNG::RNG(bool printSeed)
    :
    _seed(0), _printSeed(printSeed)
{
    InitWithRandomSeed();
}

void skr::pcg::RNG::SetRandomSeed()
{
    InitWithRandomSeed();
}

void skr::pcg::RNG::SetSeed(uint32_t seed)
{
    InitWithFixedSeed(seed);
}

void skr::pcg::RNG::SetPrintSeed(bool printSeed)
{
    _printSeed = printSeed;
}

int skr::pcg::RNG::GetRandomInteger(const int lower, const int upper)
{
    std::uniform_int_distribution<int> dist(lower, upper);
    int res = dist(_randomEngine);
    return res;
}

float skr::pcg::RNG::GetRandomFloat(const float lower, const float upper)
{
    std::uniform_real_distribution<float> dist(lower, upper);
    float res = dist(_randomEngine);
    return res;
}

double skr::pcg::RNG::GetRandomDouble(const double lower, const double upper)
{
    std::uniform_real_distribution<double> dist(lower, upper);
    double res = dist(_randomEngine);
    return res;
}

void skr::pcg::RNG::InitWithRandomSeed()
{
    _seed = GenerateViableSeed();

    if (_printSeed)
        std::cout << "Seed: " << _seed << std::endl;

    _randomEngine = std::mt19937(_seed);
}

void skr::pcg::RNG::InitWithFixedSeed(uint32_t seed)
{
    _seed = seed;

    if (!CheckViableSeed(_seed))
    {
        std::cout << "Invalid seed found, generating new one" << std::endl;

        _seed = GenerateViableSeed();
    }

    if (_printSeed)
        std::cout << "Seed: " << _seed << std::endl;

    _randomEngine = std::mt19937(_seed);
}

bool skr::pcg::RNG::CheckViableSeed(uint32_t seed)
{
    for (const auto& seedBL : _seedBlacklist)
    {
        if (seedBL == seed)
        {
            return false;
        }
    }

    return true;
}

uint32_t skr::pcg::RNG::GenerateViableSeed()
{
    uint32_t seed;
    bool viableSeedFound = false;

    while (!viableSeedFound)
    {
        seed = static_cast<uint32_t>(std::chrono::high_resolution_clock::now().time_since_epoch().count());

        if (CheckViableSeed(seed))
            viableSeedFound = true;
    }

    return seed;
}
