#include "..\pch.h"

#include "..\include\pcg\Target.h"

skr::pcg::Target::Target(TargetMovement mov)
    : _position(Vec3{ 0.0f, 0.0f, 0.0f }), _width(PCG_DEFAULT_TARGET_SIZE), _height(PCG_DEFAULT_TARGET_SIZE), _length(PCG_DEFAULT_TARGET_SIZE),
    _movement(mov)
{
}

skr::pcg::Target::Target(Vec3 pos, TargetMovement mov)
    : _position(pos), _width(PCG_DEFAULT_TARGET_SIZE), _height(PCG_DEFAULT_TARGET_SIZE), _length(PCG_DEFAULT_TARGET_SIZE),
    _movement(mov)
{
}

skr::pcg::Target::Target(Vec3 pos, float w, float h, float l, TargetMovement mov)
    : _position(pos), _width(w), _height(h), _length(l),
    _movement(mov)
{
}

skr::pcg::Target::Target(Vec3 pos, Vec3 size, TargetMovement mov)
    : _position(pos), _width(size[0]), _height(size[1]), _length(size[2]),
    _movement(mov)
{
}
