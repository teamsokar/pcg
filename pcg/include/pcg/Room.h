#ifndef SKR_PCG_ROOM
#define SKR_PCG_ROOM

#include <array>
#include <random>
#include <vector>

#include "Definitions.h"
#include "rng.h"
#include "Target.h"

namespace skr
{
    namespace pcg
    {
        //! enum DoorDirection
        //! specifying direction to which the doors elads
        enum struct DoorDirection { TOP, TOP_RIGHT, RIGHT, BOTTOM_RIGHT, BOTTOM, BOTTOM_LEFT, LEFT, TOP_LEFT };

        //! prints readable string of specified door direction
        //! \param d specified door direction
        //! \return human readable string of specified DoorDirection
        std::string GetReadbleDoorDirection(DoorDirection d);

        //! returns opposite direction of given room direction
        //! \param dir DoorDirection to be inverted
        //! \return opposite Door Direction
        DoorDirection InvertRoomDir(DoorDirection dir);

        //! enum for types of rooms
        enum struct RoomType
        { 
            Room,       //!< regular room filled with targest and light sources
            Hallway,    //!< connecting rooms, no targets or lights
            Start,      //!< place player starts in
            Finish      //!< place to finished the game
        };

        //! enum for types of faces
        enum struct FaceType
        {
            Wall,       //!< Wall, with or wihout door
            Ceiling,    //! ceilling
            Floor       //! floor
        };

        //! \class Room
        //! \brief specifies position, extends and relations to neighboring rooms
        class __declspec(dllexport) Room
        {
        public:
            Point3d _center; //!< center of room
            float _width; //!< width of room
            float _length; //!< length of room
            float _height; //!< height rom room

            RoomType _roomType; //!< type of room
#pragma warning(disable:4251) // only using types from standard library, warning can be ignored. Source: https://docs.microsoft.com/en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-1-c4251 
            std::vector<DoorDirection> _doors; //!< doors leading to hallways or special rooms
            std::vector<Room> _adjecentRooms; //!< list of adjecent rooms
            std::vector<Target> _targets; //!< list of targets in room
#pragma warning(default:4251)

        public:
            //! default constrcutor
            //! uses default values: Center at 0/0/0, Typr Room, width and lenght 5.0, height PCG_DEFAULT_ROOM_HEIGHT
            Room();

            //! Constructor
            //! \param rt Type of new room
            //! \param center Center of new room
            //! \param width Width of new room
            //! \param length Length of new room
            //! \param height HEIGHT of new room. Default PCG_DEFAULT_ROOM_HEIGHT
            Room(RoomType rt, Point3d center, float width, float length, float height = PCG_DEFAULT_ROOM_HEIGHT);

        public:
            // Bottom Side
            inline Point3d LLB(); //!< Point: LowerLeftBottom
            inline Point3d URB(); //!< Point: UpperRightBottom
            inline Point3d ULB(); //!< Point: UpperLeftBottom
            inline Point3d LRB(); //!< Point: LowerRightBottom

            // Top Side
            inline Point3d LLT(); //!< Point: LowerLeftTop
            inline Point3d URT(); //!< Point: UpperRightTop
            inline Point3d ULT(); //!< Point: UpperLeftTop
            inline Point3d LRT(); //!< Point: LowerRightTop

            //! Gets a string of the measurements of this room in a human readable way
            //! \param extensive true to enable more detailed output, default false
            //! return string with human readable text
            std::string PrintRoomMeasurements(bool extensive = false);

            //! Generates room 3D vertices for this room
            //! array of indices is present, but empty
            //! return list of faces
            std::vector<Face> GenerateGeometry3D();

            //! Generates indexed 3D verticse for this room
            //! return list of faces
            std::vector<Face> GenerateGeometry3DIndexed();

            //! Sets _targets for this room with given RNG
            //! \param rng pointer to current random number generator
            void GenerateTargets(RNG* rng);

            //! Determines closed walls of this room based on given doors
            //! \param doors list of DoorDirections
            //! \return list of DoorDirections, but meaning the closed walls
            static std::vector<DoorDirection> DetermineClosedWalls(std::vector<DoorDirection> doors);

        private:
            //! Generate indices for vertices of closed walls
            //! \param closedWalls list of DoorDirections meaning the closed walls
            //! \return list of indices
            std::vector<uint32_t> GenerateClosedWallIndices3D(std::vector<DoorDirection> closedWalls);

            //! Generates faces for closed walls
            //! \param closedWalls List of DoorDirections, but meaning closed walls
            //! \param maxTextureCoordinates specifies maximum texture coordinates aligned to the faces. Defaults to 1.0/1.0/1.0
            std::vector<Face> GenerateClosedWallFaces3D(std::vector<DoorDirection> closedWalls, Vec3 maxTextureCoordinates = Vec3{1.0f, 1.0f, 1.0f});

            //! Sets color, textures and texture parameters based on type of Room for given face
            //! \param face [out] reference to a face
            //! \param ft type of face: wall, ceiling, floor
            void SetRoomTexture(Face& face, FaceType ft);

            //! Generates targets for this give with given RNG
            //! makes sure that targets don't overlap, even when moving
            //! param rng pointer to current random number generator
            //! returns list of targets
            std::vector<Target> GenerateTargets2(RNG* rng);

        };
    }
}

#endif // !SKR_PCG_ROOM
