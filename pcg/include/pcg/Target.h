#ifndef  SKR_PCG_TARGET_H
#define  SKR_PCG_TARGET_H

#include "..\..\pch.h"

#include "Definitions.h"

namespace skr
{
    namespace pcg
    {
        //! \class TargetMovement
        //! \brief specifies parameters how a target can move during gameplay
        struct __declspec(dllexport) TargetMovement
        {
            Vec3 axis; //!< axis on which target can move
            float extend; //!< maximum length a target can move in one direction

            //! \brief default constructor
            //! sets axis to (0,0,0) (no movement) and extend to default
            TargetMovement()
                : axis(Vec3{ 0.0f, 0.0f, 0.0f }), extend(PCG_DEFAULT_TARGET_MOVEMENT_MAX_EXTEND)
            {};

            //! \brief constructor
            //! \param a axis to move on
            //! \param e extend to move
            TargetMovement(Vec3 a, float e)
                : axis(a), extend(e)
            {}

            //! \brief constructor
            //! \param dir sets direction: 0 = axis; 1 = y-axis; 2 = z-axis
            //! \param e extend to move
            TargetMovement(uint8_t dir, float e)
                : extend(e)
            {
                switch (dir)
                {
                case 0:
                    axis = Vec3{ 1.0f, 0.0f, 0.0f };
                    break;
                case 1:
                    axis = Vec3{ 0.0f, 1.0f, 0.0f };
                    break;
                case 2:
                    axis = Vec3{ 0.0f, 0.0f, 1.0f };
                    break;
                default:
                    axis = Vec3{ 0.0f, 0.0f, 0.0f };
                }
            }
        };

        //! \class Target
        //! \brief specifies are target in a level to hit
        class __declspec(dllexport) Target
        {
        public:
            Vec3 _position; //!< position
            float _width;   //!< extend of the target in x-axis
            float _height;  //!< extend of the target in y-axis
            float _length;  //!< extend of the target in z-axis

            TargetMovement _movement; //!< target movement

            //! \brief default constructor
            //! \param mov target movement, default uses its default constructor
            Target(TargetMovement mov = TargetMovement());

            //! \brief constructor
            //! \param pos position of the target
            //! \param mov target movement, default uses its default constructor
            Target(Vec3 pos, TargetMovement mov = TargetMovement());

            //! \brief constructor
            //! \param pos position of the target
            //! \param w width
            //! \param h height
            //! \param l length
            //! \param mov target movement, default uses its default constructor
            Target(Vec3 pos, float w, float h, float l, TargetMovement mov = TargetMovement());

            //! \brief constructor
            //! \param pos position of the target
            //! \param size width, height, length
            //! \param mov target movement, default uses its default constructor
            Target(Vec3 pos, Vec3 size, TargetMovement mov = TargetMovement());

        };
    }
}

#endif // !SKR_PCG_TARGET_H

