#include "..\pch.h"

#include <ctime>
#include <string>
#include <array>

#include "..\include\pcg\Room.h"
#include "..\include\pcg\Definitions.h"
#include "..\include\pcg\rng.h"

namespace skr
{
    namespace pcg
    {

#pragma region Constructors
        Room::Room()
            : _center(Point3d{ 0.0f, 0.0f, 0.0f }),
            _width(5.0f), _length(5.0f), _height(PCG_DEFAULT_ROOM_HEIGHT),
            _roomType(RoomType::Room)
        {
        }

        Room::Room(RoomType rt, Point3d center, float width, float length, float height)
            : _center(center),
            _width(width), _length(length), _height(height),
            _roomType(rt)
        {
        }

#pragma endregion

#pragma region inline functions for corner points bottom and top plane
        Point3d Room::LLB()
        {
            return Point3d{ _center[0] - _width * 0.5f, _center[1] - _height * 0.5f, _center[2] - _length * 0.5f };
        }

        Point3d Room::URB()
        {
            return Point3d{ _center[0] + _width * 0.5f, _center[1] - _height * 0.5f, _center[2] + _length * 0.5f };
        }

        Point3d Room::ULB()
        {
            return Point3d{ _center[0] - _width * 0.5f, _center[1] - _height * 0.5f, _center[2] + _length * 0.5f };
        }

        Point3d Room::LRB()
        {
            return Point3d{ _center[0] + _width * 0.5f, _center[1] - _height * 0.5f, _center[2] - _length * 0.5f };
        }

        inline Point3d Room::LLT()
        {
            return Point3d{ _center[0] - _width * 0.5f, _center[1] + _height * 0.5f, _center[2] - _length * 0.5f };
        }

        inline Point3d Room::URT()
        {
            return Point3d{ _center[0] + _width * 0.5f, _center[1] + _height * 0.5f, _center[2] + _length * 0.5f };
        }

        inline Point3d Room::ULT()
        {
            return Point3d{ _center[0] - _width * 0.5f, _center[1] + _height * 0.5f, _center[2] + _length * 0.5f };
        }

        inline Point3d Room::LRT()
        {
            return Point3d{ _center[0] + _width * 0.5f, _center[1] + _height * 0.5f, _center[2] - _length * 0.5f };
        }
#pragma endregion

        std::string Room::PrintRoomMeasurements(bool extensive)
        {
            std::string str;

            switch (_roomType)
            {
            case RoomType::Room:
                str += "Room: ";
                break;

            case RoomType::Hallway:
                str += "Hallway: ";
                break;
            case RoomType::Start:
                str += "Startbox: ";
                break;
            case RoomType::Finish:
                str += "Finishbox: ";
                break;
            default:
                break;
            }

            if (extensive)
            {
                str += "Center: ";
            }

            for (auto item : _center)
            {
                str += std::to_string(item) + ',';
            }

            str += ' ';

            if (extensive)
                str += "Dimensions: ";

            str += std::to_string(_width) + 'x' + std::to_string(_length) + 'x' + std::to_string(_height) + '\n';

            if (extensive)
            {
                str += "Size: ";
                str += std::to_string(_width * _length) + '\n';
            }

            if (extensive)
            {
                str += "Doors: ";
                for (auto& d : _doors)
                    str += GetReadbleDoorDirection(d) + ", ";

                str += "\n";
            }

            if (_roomType == RoomType::Room)
            {
                str.append("Targets: ");
                str += "Number of Targets: " + std::to_string(_targets.size()) + '\n';

                if (extensive)
                {
                    for (auto& t : _targets)
                    {
                        str += "\tPosition: " + std::to_string(t._position[0]) + ',' + std::to_string(t._position[1]) + ',' + std::to_string(t._position[2]);
                        str += " Size: " + std::to_string(t._width) + 'x' + std::to_string(t._height) + 'x' + std::to_string(t._length);
                        str += '\n';
                    }
                }
            }

            return str;
        }

        std::vector<Face> Room::GenerateGeometry3D()
        {
            std::vector<Face> geometry;
            Face face;
            Vertex vertex;

            auto maxTextureCoordinates = std::array<float, 3>{ _width, _height, _length };

#pragma region Bottom Face
            SetRoomTexture(face, FaceType::Floor);

            vertex.Normal = DefaultNormal_Bottom;

            vertex.Position = URB();
            vertex.TexCoords = Vec2{ maxTextureCoordinates[0], maxTextureCoordinates[2] };
            face.vertices.push_back(vertex);

            vertex.Position = LRB();
            vertex.TexCoords = Vec2{ maxTextureCoordinates[0], 0.0f };
            face.vertices.push_back(vertex);

            vertex.Position = LLB();
            vertex.TexCoords = Vec2{ 0.0f, 0.0f };
            face.vertices.push_back(vertex);


            vertex.Position = URB();
            vertex.TexCoords = Vec2{ maxTextureCoordinates[0], maxTextureCoordinates[2] };
            face.vertices.push_back(vertex);

            vertex.Position = LLB();
            vertex.TexCoords = Vec2{ 0.0f, 0.0f };
            face.vertices.push_back(vertex);

            vertex.Position = ULB();
            vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[2] };
            face.vertices.push_back(vertex);

            geometry.push_back(face);
            face.vertices.clear();
#pragma endregion

#pragma region Top Face
            SetRoomTexture(face, FaceType::Ceiling);

            vertex.Position = URT();
            vertex.Normal = DefaultNormal_Top;
            vertex.TexCoords = Vec2{ maxTextureCoordinates[0], maxTextureCoordinates[2] };
            face.vertices.push_back(vertex);

            vertex.Position = LLT();
            vertex.TexCoords = Vec2{ 0.0, 0.0f };
            face.vertices.push_back(vertex);

            vertex.Position = LRT();
            vertex.TexCoords = Vec2{ maxTextureCoordinates[0], 0.0f };
            face.vertices.push_back(vertex);



            vertex.Position = URT();
            vertex.TexCoords = Vec2{ maxTextureCoordinates[0], maxTextureCoordinates[2] };
            face.vertices.push_back(vertex);

            vertex.Position = ULT();
            vertex.TexCoords = Vec2{ 0.0, maxTextureCoordinates[2] };
            face.vertices.push_back(vertex);

            vertex.Position = LLT();
            vertex.TexCoords = Vec2{ 0.0, 0.0f };
            face.vertices.push_back(vertex);

            geometry.push_back(face);
            face.vertices.clear();
#pragma endregion
    
            SetRoomTexture(face, FaceType::Wall);
            auto closedWalls = DetermineClosedWalls(_doors);
            auto cw = GenerateClosedWallFaces3D(closedWalls, maxTextureCoordinates);
            geometry.insert(geometry.end(), cw.begin(), cw.end());

            for (auto& d : _doors)
            {
                switch (d)
                {
                case DoorDirection::TOP: // back
                {
                    float dist = (_width - PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;

                    Vec3 d1{ ULT()[0] + dist, ULT()[1], ULT()[2] };
                    Vec3 d2{ ULB()[0] + dist, ULB()[1], ULB()[2] };
                    Vec3 d3{ URB()[0] - dist, URB()[1], URB()[2] };
                    Vec3 d4{ URT()[0] - dist, URT()[1], URT()[2] };

                    vertex.Normal = DefaultNormal_Back;

                    // T1
                    vertex.Position = URB();
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = d3;
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = d4;
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    // T2
                    vertex.Position = URB();
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = d4;
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = URT();
                    vertex.TexCoords = Vec2{ dist, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);



                    // treat both sides as separate faces, similar to closed walls
                    geometry.push_back(face);
                    face.vertices.clear();



                    // T3
                    vertex.Position = d2;
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = ULB();
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = ULT();
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    // T4
                    vertex.Position = d2;
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = ULT();
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = d1;
                    vertex.TexCoords = Vec2{ dist, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    geometry.push_back(face);
                    face.vertices.clear();
                    break;
                }

                case DoorDirection::RIGHT: // right
                {
                    float dist = (_length - PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;

                    Vec3 d1{ URT()[0], URT()[1], URT()[2] - dist };
                    Vec3 d2{ URB()[0], URB()[1], URB()[2] - dist };
                    Vec3 d3{ LRB()[0], LRB()[1], LRB()[2] + dist };
                    Vec3 d4{ LRT()[0], LRT()[1], LRT()[2] + dist };

                    vertex.Normal = DefaultNormal_Right;

                    // T1
                    vertex.Position = LRB();
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = d3;
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = d4;
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    // T2
                    vertex.Position = LRB();
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = d4;
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = LRT();
                    vertex.TexCoords = Vec2{ dist, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);



                    // treat both sides as separate faces, similar to closed walls
                    geometry.push_back(face);
                    face.vertices.clear();



                    // T3
                    vertex.Position = d2;
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = URB();
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = URT();
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1]  };
                    face.vertices.push_back(vertex);

                    // T4
                    vertex.Position = d2;
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = URT();
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = d1;
                    vertex.TexCoords = Vec2{ dist, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    geometry.push_back(face);
                    face.vertices.clear();
                    break;
                }

                case DoorDirection::BOTTOM: // front
                {
                    float dist = (_width - PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;

                    Vec3 d1{ LRT()[0] - dist, LRT()[1], LRT()[2] };
                    Vec3 d2{ LRB()[0] - dist, LRB()[1], LRB()[2] };
                    Vec3 d3{ LLB()[0] + dist, LLB()[1], LLB()[2] };
                    Vec3 d4{ LLT()[0] + dist, LLT()[1], LLT()[2] };

                    vertex.Normal = DefaultNormal_Front;

                    // T1
                    vertex.Position = LLB();
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = d3;
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = d4;
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    // T2
                    vertex.Position = LLB();
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = d4;
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = LLT();
                    vertex.TexCoords = Vec2{ dist, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);


                    // treat both sides as separate faces, similar to closed walls
                    geometry.push_back(face);
                    face.vertices.clear();


                    // T3
                    vertex.Position = d2;
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = LRB();
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = LRT();
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    // T4
                    vertex.Position = d2;
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = LRT();
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = d1;
                    vertex.TexCoords = Vec2{ dist, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    geometry.push_back(face);
                    face.vertices.clear();
                    break;
                }

                case DoorDirection::LEFT: // left
                {
                    float dist = (_length - PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;

                    Vec3 d1{ LLT()[0], LLT()[1], LLT()[2] + dist };
                    Vec3 d2{ LLB()[0], LLB()[1], LLB()[2] + dist };
                    Vec3 d3{ ULB()[0], ULB()[1], ULB()[2] - dist };
                    Vec3 d4{ ULT()[0], ULT()[1], ULT()[2] - dist };

                    vertex.Normal = DefaultNormal_Left;

                    // T1
                    vertex.Position = ULB();
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = d3;
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = d4;
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    // T2
                    vertex.Position = ULB();
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = d4;
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = ULT();
                    vertex.TexCoords = Vec2{ dist, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);


                    // treat both sides as separate faces, similar to closed walls
                    geometry.push_back(face);
                    face.vertices.clear();


                    // T3
                    vertex.Position = d2;
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = LLB();
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = LLT();
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    // T4
                    vertex.Position = d2;
                    vertex.TexCoords = Vec2{ dist, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = LLT();
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = d1;
                    vertex.TexCoords = Vec2{ dist, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    geometry.push_back(face);
                    face.vertices.clear();
                    break;
                }

                default:
                    break;
                }
            }

            return geometry;
        }

        std::vector<Face> Room::GenerateGeometry3DIndexed()
        {
            std::vector<Face> faces;
            Face face;
            Vertex vertex;
            uint32_t nextIndex = 8;

#pragma region Points
            vertex.Position = URB();
            vertex.Normal = Vec3{ 0.0f, 1.0f, 0.0f };
            vertex.TexCoords = Vec2{ 0.0f, 0.0f };
            face.vertices.push_back(vertex);
            uint32_t urbpos = face.vertices.size() - 1;

            vertex.Position = LRB();
            vertex.Normal = Vec3{ 0.0f, 1.0f, 0.0f };
            vertex.TexCoords = Vec2{ 0.0f, 0.0f };
            face.vertices.push_back(vertex);
            uint32_t lrbpos = face.vertices.size() - 1;

            vertex.Position = LLB();
            vertex.Normal = Vec3{ 0.0f, 1.0f, 0.0f };
            vertex.TexCoords = Vec2{ 0.0f, 0.0f };
            face.vertices.push_back(vertex);
            uint32_t llbpos = face.vertices.size() - 1;

            vertex.Position = ULB();
            vertex.Normal = Vec3{ 0.0f, 1.0f, 0.0f };
            vertex.TexCoords = Vec2{ 0.0f, 0.0f };
            face.vertices.push_back(vertex);
            uint32_t ulbpos = face.vertices.size() - 1;


            vertex.Position = URT();
            vertex.Normal = Vec3{ 0.0f, -1.0f, 0.0f };
            vertex.TexCoords = Vec2{ 0.0f, 0.0f };
            face.vertices.push_back(vertex);
            uint32_t urtpos = face.vertices.size() - 1;

            vertex.Position = LRT();
            vertex.Normal = Vec3{ 0.0f, -1.0f, 0.0f };
            vertex.TexCoords = Vec2{ 0.0f, 0.0f };
            face.vertices.push_back(vertex);
            uint32_t lrtpos = face.vertices.size() - 1;

            vertex.Position = LLT();
            vertex.Normal = Vec3{ 0.0f, -1.0f, 0.0f };
            vertex.TexCoords = Vec2{ 0.0f, 0.0f };
            face.vertices.push_back(vertex);
            uint32_t lltpos = face.vertices.size() - 1;

            vertex.Position = ULT();
            vertex.Normal = Vec3{ 0.0f, -1.0f, 0.0f };
            vertex.TexCoords = Vec2{ 0.0f, 0.0f };
            face.vertices.push_back(vertex);
            uint32_t ultpos = face.vertices.size() - 1;

#pragma endregion

            if (_roomType == RoomType::Room)
            {
                Vec3 color{ 0.5f, 0.5f, 0.5f };

                //Bottom
                face.indices.insert(face.indices.end(), { urbpos, lrbpos, llbpos });
                face.indices.insert(face.indices.end(), { urbpos, llbpos, ulbpos });
                face.color = color;
                faces.push_back(face);
                face.indices.clear();

                // Top
                face.indices.insert(face.indices.end(), { urtpos, lltpos, lrtpos });
                face.indices.insert(face.indices.end(), { urtpos, ultpos, lltpos });
                face.color = color;
                faces.push_back(face);
                face.indices.clear();

                // Walls
                auto closedWalls = DetermineClosedWalls(_doors);
                auto cw = GenerateClosedWallIndices3D(closedWalls);
                face.indices.insert(face.indices.end(), cw.begin(), cw.end());
                face.color = color;
                faces.push_back(face);
                face.indices.clear();

                for (auto d : _doors)
                {
                    switch (d)
                    {
                    case DoorDirection::TOP: // back
                    {
                        float dist = (_width - PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;

                        Vec3 d1{ ULT()[0] + dist, ULT()[1], ULT()[2] };
                        Vec3 d2{ ULB()[0] + dist, ULB()[1], ULB()[2] };
                        Vec3 d3{ URB()[0] - dist, URB()[1], URB()[2] };
                        Vec3 d4{ URT()[0] - dist, URT()[1], URT()[2] };

                        vertex.Position = d1;
                        vertex.Normal = Vec3{ 0.0f, 0.0f, -1.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d1pos = face.vertices.size() - 1;

                        vertex.Position = d2;
                        vertex.Normal = Vec3{ 0.0f, 0.0f, -1.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d2pos = face.vertices.size() - 1;

                        vertex.Position = d3;
                        vertex.Normal = Vec3{ 0.0f, 0.0f, -1.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d3pos = face.vertices.size() - 1;

                        vertex.Position = d4;
                        vertex.Normal = Vec3{ 0.0f, 0.0f, -1.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d4pos = face.vertices.size() - 1;


                        face.indices.insert(face.indices.end(),
                            { urbpos, d3pos, d4pos,
                              urbpos, d4pos, urtpos,

                              d2pos, ulbpos, ultpos,
                              d2pos, ultpos, d1pos
                            }
                        );

                        faces.push_back(face);
                        face.indices.clear();

                        break;
                    }

                    case DoorDirection::RIGHT: // right
                    {
                        float dist = (_length - PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;

                        Vec3 d1{ URT()[0], URT()[1], URT()[2] - dist };
                        Vec3 d2{ URB()[0], URB()[1], URB()[2] - dist };
                        Vec3 d3{ LRB()[0], LRB()[1], LRB()[2] + dist };
                        Vec3 d4{ LRT()[0], LRT()[1], LRT()[2] + dist };

                        vertex.Position = d1;
                        vertex.Normal = Vec3{ -1.0f, 0.0f, 0.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d1pos = face.vertices.size() - 1;

                        vertex.Position = d2;
                        vertex.Normal = Vec3{ -1.0f, 0.0f, 0.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d2pos = face.vertices.size() - 1;

                        vertex.Position = d3;
                        vertex.Normal = Vec3{ -1.0f, 0.0f, 0.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d3pos = face.vertices.size() - 1;

                        vertex.Position = d4;
                        vertex.Normal = Vec3{ -1.0f, 0.0f, 0.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d4pos = face.vertices.size() - 1;

                        face.indices.insert(face.indices.end(),
                            { lrbpos, d3pos,
                                d4pos, lrbpos, d4pos,

                                lrtpos, d2pos, urbpos,
                                urtpos, d2pos, urtpos, d1pos
                            }
                        );

                        faces.push_back(face);
                        face.indices.clear();

                        break;
                    }

                    case DoorDirection::BOTTOM: // front
                    {
                        float dist = (_width - PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;

                        Vec3 d1{ LRT()[0] - dist, LRT()[1], LRT()[2] };
                        Vec3 d2{ LRB()[0] - dist, LRB()[1], LRB()[2] };
                        Vec3 d3{ LLB()[0] + dist, LLB()[1], LLB()[2] };
                        Vec3 d4{ LLT()[0] + dist, LLT()[1], LLT()[2] };

                        vertex.Position = d1;
                        vertex.Normal = Vec3{ 0.0f, 0.0f, 1.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d1pos = face.vertices.size() - 1;

                        vertex.Position = d2;
                        vertex.Normal = Vec3{ 0.0f, 0.0f, 1.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d2pos = face.vertices.size() - 1;

                        vertex.Position = d3;
                        vertex.Normal = Vec3{ 0.0f, 0.0f, 1.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d3pos = face.vertices.size() - 1;

                        vertex.Position = d4;
                        vertex.Normal = Vec3{ 0.0f, 0.0f, 1.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d4pos = face.vertices.size() - 1;

                        face.indices.insert(face.indices.end(),
                            {
                                llbpos, d3pos, d4pos,
                                llbpos, d4pos, lltpos,

                                d2pos, lrbpos, lrtpos,
                                d2pos, lrtpos, d1pos
                            }
                        );

                        faces.push_back(face);
                        face.indices.clear();

                        break;
                    }

                    case DoorDirection::LEFT: // left
                    {
                        float dist = (_length - PCG_DEFAULT_ROOM_STACKING_OFFSET) * 0.5f;

                        Vec3 d1{ LLT()[0], LLT()[1], LLT()[2] + dist };
                        Vec3 d2{ LLB()[0], LLB()[1], LLB()[2] + dist };
                        Vec3 d3{ ULB()[0], ULB()[1], ULB()[2] - dist };
                        Vec3 d4{ ULT()[0], ULT()[1], ULT()[2] - dist };

                        vertex.Position = d1;
                        vertex.Normal = Vec3{ 1.0f, 0.0f, 0.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d1pos = face.vertices.size() - 1;

                        vertex.Position = d2;
                        vertex.Normal = Vec3{ 1.0f, 0.0f, 0.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d2pos = face.vertices.size() - 1;

                        vertex.Position = d3;
                        vertex.Normal = Vec3{ 1.0f, 0.0f, 0.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d3pos = face.vertices.size() - 1;

                        vertex.Position = d4;
                        vertex.Normal = Vec3{ 1.0f, 0.0f, 0.0f };
                        vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                        face.vertices.push_back(vertex);
                        uint32_t d4pos = face.vertices.size() - 1;

                        face.indices.insert(face.indices.end(),
                            {
                                ulbpos, d3pos, d4pos,
                                ulbpos, d4pos, ultpos,

                                d2pos, llbpos, lltpos,
                                d2pos, lltpos, d1pos
                            }
                        );

                        faces.push_back(face);
                        face.indices.clear();

                        break;
                    }

                    default:
                        break;
                    }
                }
            }
            else if (_roomType == RoomType::Hallway)
            {
                Vec3 color{ 1.0f, 0.0f, 1.0f };

                //Bottom
                face.indices.insert(face.indices.end(), { urbpos, lrbpos, llbpos });
                face.indices.insert(face.indices.end(), { urbpos, llbpos, ulbpos });
                face.color = color;
                faces.push_back(face);
                face.indices.clear();

                // Top
                face.indices.insert(face.indices.end(), { urtpos, lltpos, lrtpos });
                face.indices.insert(face.indices.end(), { urtpos, ultpos, lltpos });
                face.color = color;
                faces.push_back(face);
                face.indices.clear();

                // Walls
                auto closedWalls = DetermineClosedWalls(_doors);
                auto cw = GenerateClosedWallIndices3D(closedWalls);
                face.indices.insert(face.indices.end(), cw.begin(), cw.end());
                face.color = color;
                faces.push_back(face);
                face.indices.clear();
            }
            else if (_roomType == RoomType::Start)
            {
                Vec3 color{ 0.0f, 0.0f, 1.0f };

                //Bottom
                face.indices.insert(face.indices.end(), { urbpos, lrbpos, llbpos });
                face.indices.insert(face.indices.end(), { urbpos, llbpos, ulbpos });
                face.color = color;
                faces.push_back(face);
                face.indices.clear();

                // Top
                face.indices.insert(face.indices.end(), { urtpos, lltpos, lrtpos });
                face.indices.insert(face.indices.end(), { urtpos, ultpos, lltpos });
                face.color = color;
                faces.push_back(face);
                face.indices.clear();

                // Walls
                auto closedWalls = DetermineClosedWalls(_doors);
                auto cw = GenerateClosedWallIndices3D(closedWalls);
                face.indices.insert(face.indices.end(), cw.begin(), cw.end());
                face.color = color;
                faces.push_back(face);
                face.indices.clear();
            }
            else if (_roomType == RoomType::Finish)
            {
                Vec3 color{ 0.0f, 1.0f, 1.0f };

                //Bottom
                face.indices.insert(face.indices.end(), { urbpos, lrbpos, llbpos });
                face.indices.insert(face.indices.end(), { urbpos, llbpos, ulbpos });
                face.color = color;
                faces.push_back(face);
                face.indices.clear();

                // Top
                face.indices.insert(face.indices.end(), { urtpos, lltpos, lrtpos });
                face.indices.insert(face.indices.end(), { urtpos, ultpos, lltpos });
                face.color = color;
                faces.push_back(face);
                face.indices.clear();

                // Walls
                auto closedWalls = DetermineClosedWalls(_doors);
                auto cw = GenerateClosedWallIndices3D(closedWalls);
                face.indices.insert(face.indices.end(), cw.begin(), cw.end());
                face.color = color;
                faces.push_back(face);
                face.indices.clear();
            }

            return faces;
        }

        void Room::GenerateTargets(RNG* rng)
        {
            _targets = GenerateTargets2(rng);
        }

        std::vector<Target> Room::GenerateTargets2(RNG* rng)
        {
            if (_roomType == RoomType::Hallway)
                return std::vector<Target>();

            std::vector<Target> targets;

            size_t i(0);

            uint32_t noTargets = MIN_TARGETS;
            auto room_size = static_cast<uint32_t>(_width * _length);
            auto targetsPerSize = room_size / TARGETS_PER_SQUARE_SIZE;

            if (targetsPerSize > noTargets)
                noTargets = targetsPerSize;

            while (i < noTargets)
            {
                float x = rng->GetRandomFloat(-(_width * 0.5f - PCG_DEFAULT_TARGET_SIZE), _width * 0.5f - PCG_DEFAULT_TARGET_SIZE);
                float y = 0.0f;
                float z = rng->GetRandomFloat(-(_length * 0.5f - PCG_DEFAULT_TARGET_SIZE), _length * 0.5f - PCG_DEFAULT_TARGET_SIZE);

                uint8_t dir = rng->GetRandomInteger(0, 2); //X, Y, Z
                float extend;
                if (dir != 1)
                    extend = rng->GetRandomFloat(0, PCG_DEFAULT_TARGET_MOVEMENT_MAX_EXTEND);
                else
                    extend = rng->GetRandomFloat(0, PCG_DEFAULT_TARGET_MOVEMENT_MAX_EXTEND_VERTICAL);

                // Overlap check
                float lx = _center[0] - x;
                float ux = _center[0] + x;

                if (dir == 0)
                {
                    lx -= extend;
                    ux += extend;
                }

                float lz = _center[2] - z;
                float uz = _center[2] + z;

                if (dir == 2)
                {
                    lz -= extend;
                    uz += extend;
                }

                for (auto t : targets)
                {
                    if ((t._position[0] > lx && t._position[0] < ux) && (t._position[2] > lz && t._position[2] < uz))
                        continue;
                }

                ++i;

                TargetMovement mov = TargetMovement(dir, extend);

                targets.push_back(Target(Vec3{ _center[0] + x, _center[1] + y, _center[2] + z }, mov));
            }

            return targets;
        }

        std::vector<uint32_t> Room::GenerateClosedWallIndices3D(std::vector<DoorDirection> closedWalls)
        {
            std::vector<uint32_t> indices;

            for (auto cw : closedWalls)
            {
                switch (cw)
                {
                case DoorDirection::TOP: // back
                {
                    indices.insert(indices.end(), { 4, 0, 3 });
                    indices.insert(indices.end(), { 4, 3, 7 });
                    break;
                }

                case DoorDirection::RIGHT: // right
                {
                    indices.insert(indices.end(), { 5, 1, 0 });
                    indices.insert(indices.end(), { 5, 0, 4 });
                    break;
                }

                case DoorDirection::BOTTOM: // front
                {
                    indices.insert(indices.end(), { 5, 2, 1 });
                    indices.insert(indices.end(), { 5, 6, 2 });
                    break;
                }

                case DoorDirection::LEFT: // left
                {
                    indices.insert(indices.end(), { 6, 3, 2 });
                    indices.insert(indices.end(), { 6, 7, 3 });
                    break;
                }

                default:
                    break;
                }
            }

            return indices;
        }

        std::vector<Face> Room::GenerateClosedWallFaces3D(std::vector<DoorDirection> closedWalls, Vec3 maxTextureCoordinates)
        {
            std::vector<Face> geometry;
            Face face;
            Vertex vertex;

            SetRoomTexture(face, FaceType::Wall);

            for (auto cw : closedWalls)
            {
                switch (cw)
                {
                case DoorDirection::TOP: // back
                {
                    vertex.Normal = DefaultNormal_Back;

                    vertex.Position = URT(); // 4
                    vertex.TexCoords = Vec2{ maxTextureCoordinates[0], maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = URB(); // 0
                    vertex.TexCoords = Vec2{ maxTextureCoordinates[0], 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = ULB(); // 3
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);




                    vertex.Position = URT(); // 4
                    vertex.TexCoords = Vec2{ maxTextureCoordinates[0], maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = ULB(); // 3
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = ULT(); // 7
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);



                    geometry.push_back(face);
                    face.vertices.clear();
                    break;
                }

                case DoorDirection::RIGHT: // right
                {
                    // note on texture coordiantes: they might seem switched (S/T-direction), but this is to preserve winding order -> consider looking at them from the middle of the room outward

                    vertex.Normal = DefaultNormal_Right;


                    vertex.Position = LRT(); // 5
                    vertex.TexCoords = Vec2{ maxTextureCoordinates[2], maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = LRB(); // 1
                    vertex.TexCoords = Vec2{ maxTextureCoordinates[2], 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = URB(); // 0
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);



                    vertex.Position = LRT(); // 5
                    vertex.TexCoords = Vec2{ maxTextureCoordinates[2], maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = URB(); // 0
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = URT(); // 4
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);



                    geometry.push_back(face);
                    face.vertices.clear();
                    break;
                }

                case DoorDirection::BOTTOM: // front
                {
                    vertex.Normal = DefaultNormal_Bottom;


                    vertex.Position = LRT(); // 5
                    vertex.TexCoords = Vec2{ maxTextureCoordinates[0], maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = LLT(); // 6
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = LLB(); // 2
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);



                    vertex.Position = LRT(); // 5
                    vertex.TexCoords = Vec2{ maxTextureCoordinates[0], maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = LLB(); // 2
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = LRB(); // 1
                    vertex.TexCoords = Vec2{ maxTextureCoordinates[0], 0.0f };
                    face.vertices.push_back(vertex);



                    geometry.push_back(face);
                    face.vertices.clear();
                    break;
                }

                case DoorDirection::LEFT: // left
                {
                    // note on texture coordiantes: they might seem switched (S/T-direction), but this is to preserve winding order -> consider looking at them from the middle of the room outward

                    vertex.Normal = DefaultNormal_Left;

                    vertex.Position = ULT(); // 7
                    vertex.TexCoords = Vec2{ maxTextureCoordinates[2], maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = ULB(); // 3
                    vertex.TexCoords = Vec2{ maxTextureCoordinates[2], 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = LLB(); // 2
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);



                    vertex.Position = ULT(); // 7
                    vertex.TexCoords = Vec2{ maxTextureCoordinates[2], maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);

                    vertex.Position = LLB(); // 2
                    vertex.TexCoords = Vec2{ 0.0f, 0.0f };
                    face.vertices.push_back(vertex);

                    vertex.Position = LLT(); // 6
                    vertex.TexCoords = Vec2{ 0.0f, maxTextureCoordinates[1] };
                    face.vertices.push_back(vertex);


                    geometry.push_back(face);
                    face.vertices.clear();
                    break;
                }

                default:
                    break;
                }
            }
            return geometry;
        }

        void Room::SetRoomTexture(Face& face, FaceType ft)
        {
            switch (_roomType)
            {
            case RoomType::Room:
                // intentional fall-through as both use the same textures
            case RoomType::Hallway:
                switch (ft)
                {
                case FaceType::Wall:
                    face.color = Vec3{ 0.5f, 0.5f, 0.5f };
                    face.textureWrapParameter = OpenGLTextureWrapParameter::OGL_REPEAT;
                    face.diffuseTexturePath = std::string("textures/219.jpg");
                    face.normalTexturePath = std::string("textures/219_norm.jpg");
                    break;

                case FaceType::Ceiling:
                    face.color = Vec3{ 0.5f, 0.5f, 0.5f };
                    face.textureWrapParameter = OpenGLTextureWrapParameter::OGL_REPEAT;
                    face.diffuseTexturePath = std::string("textures/213.jpg");
                    face.normalTexturePath = std::string("textures/213_norm.jpg");
                    break;

                case FaceType::Floor:
                    face.textureWrapParameter = OpenGLTextureWrapParameter::OGL_REPEAT;
                    face.diffuseTexturePath = std::string("textures/Flooring_Stone_001_COLOR.png");
                    face.normalTexturePath = std::string("textures/Flooring_Stone_001_NRM.png");
                    break;

                default:
                    face.color = Vec3{ 0.5f, 0.5f, 0.5f };
                    face.textureWrapParameter = OpenGLTextureWrapParameter::OGL_REPEAT;
                    face.color = Vec3{ 1.0f, 0.0f, 0.0f };
                    face.diffuseTexturePath = std::string("textures/202.jpg");
                    face.normalTexturePath = std::string("textures/202_norm.jpg");
                }
                break;

            case RoomType::Start:
                face.color = Vec3{ 0.0f, 0.0f, 1.0f };
                face.textureWrapParameter = OpenGLTextureWrapParameter::OGL_REPEAT;
                face.diffuseTexturePath = std::string("textures/203.jpg");
                face.normalTexturePath = std::string("textures/203_norm.jpg");
                break;

            case RoomType::Finish:
                face.color = Vec3{ 0.0f, 1.0f, 1.0f };
                face.textureWrapParameter = OpenGLTextureWrapParameter::OGL_REPEAT;
                face.diffuseTexturePath = std::string("textures/221.jpg");
                face.normalTexturePath = std::string("textures/221_norm.jpg");
                break;

            default: // should not happe, therefore chose a color and texture that stands out
                face.color = Vec3{ 1.0f, 0.0f, 0.0f };
                face.textureWrapParameter = OpenGLTextureWrapParameter::OGL_REPEAT;
                face.diffuseTexturePath = std::string("textures/202.jpg");
                face.normalTexturePath = std::string("textures/202_norm.jpg");
            }
        }

        std::vector<DoorDirection> Room::DetermineClosedWalls(std::vector<DoorDirection> doors)
        {
            std::vector<DoorDirection> closedWalls{ DoorDirection::TOP, DoorDirection::LEFT, DoorDirection::BOTTOM, DoorDirection::RIGHT };

            for (auto d : doors)
            {
                switch (d)
                {
                case DoorDirection::TOP:
                {
                    closedWalls.erase(std::remove(closedWalls.begin(), closedWalls.end(), DoorDirection::TOP), closedWalls.end());

                    break;
                }
                case DoorDirection::RIGHT:
                {
                    closedWalls.erase(std::remove(closedWalls.begin(), closedWalls.end(), DoorDirection::RIGHT), closedWalls.end());

                    break;
                }
                case DoorDirection::BOTTOM:
                {

                    closedWalls.erase(std::remove(closedWalls.begin(), closedWalls.end(), DoorDirection::BOTTOM), closedWalls.end());

                    break;
                }
                case DoorDirection::LEFT:
                {
                    closedWalls.erase(std::remove(closedWalls.begin(), closedWalls.end(), DoorDirection::LEFT), closedWalls.end());

                    break;
                }
                default:
                    break;
                }
            }

            return closedWalls;
        }

        std::string GetReadbleDoorDirection(DoorDirection d)
        {
            switch (d)
            {
            case DoorDirection::TOP: // back
            {
                return std::string("Top");
                break;
            }

            case DoorDirection::RIGHT: // right
            {
                return std::string("Right");
                break;
            }

            case DoorDirection::BOTTOM: // front
            {
                return std::string("Bottom");
                break;
            }

            case DoorDirection::LEFT: // left
            {
                return std::string("Left");
                break;
            }

            default:
                return std::string("unknown direction");
                break;
            }
        }

        DoorDirection InvertRoomDir(DoorDirection dir)
        {
            switch (dir)
            {
            case skr::pcg::DoorDirection::TOP:
                return DoorDirection::BOTTOM;
                break;
            case skr::pcg::DoorDirection::TOP_RIGHT:
                return DoorDirection::BOTTOM_LEFT;
                break;
            case skr::pcg::DoorDirection::RIGHT:
                return DoorDirection::LEFT;
                break;
            case skr::pcg::DoorDirection::BOTTOM_RIGHT:
                return DoorDirection::TOP_RIGHT;
                break;
            case skr::pcg::DoorDirection::BOTTOM:
                return DoorDirection::TOP;
                break;
            case skr::pcg::DoorDirection::BOTTOM_LEFT:
                return DoorDirection::TOP_RIGHT;
                break;
            case skr::pcg::DoorDirection::LEFT:
                return DoorDirection::RIGHT;
                break;
            case skr::pcg::DoorDirection::TOP_LEFT:
                return DoorDirection::BOTTOM_RIGHT;
                break;
            default:
                // throw exception?
                break;
            }
        }
    }
}