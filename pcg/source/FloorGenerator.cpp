#include "..\pch.h"
#include "..\include\pcg\FloorGenerator.h"
#include "..\include\pcg\rng.h"
#include "..\include\\pcg\\Definitions.h"

#pragma region FloorGenerator

skr::pcg::FloorGenerator::FloorGenerator()
{
    _param = FloorGeneratorParameter();
    _rng.SetPrintSeed(false);
    _rng.SetRandomSeed();

    StartGeneration();
}

skr::pcg::FloorGenerator::FloorGenerator(FloorGeneratorParameter& param)
{
    _param = param;
    _rng.SetPrintSeed(_param._rng_printSeed);

    if (param._rng_randomSeed)
        _rng.SetRandomSeed();
    else
        _rng.SetSeed(_param._rng_seed);

    StartGeneration();

    // update seed if an invalid one was found and replaced
    param._rng_seed = _rng._seed;
}

std::vector<skr::pcg::Room> skr::pcg::FloorGenerator::GetAllGeometry()
{
    std::vector<Room> geometry;
    geometry.reserve(_floor.size() + _hallways.size() + 2);

    geometry.push_back(_startBox);
    geometry.insert(geometry.end(), _floor.begin(), _floor.end());
    geometry.insert(geometry.end(), _hallways.begin(), _hallways.end());
    geometry.push_back(_finishBox);

    return geometry;
}

void skr::pcg::FloorGenerator::StartGeneration()
{
    switch (_param._layoutStrategy)
    {
    case FloorLayoutStrategy::RandomPlacement:
        GenerateRoomsRandomPlacement();
        break;
    case FloorLayoutStrategy::Stacking:
        GenerateRoomsStacking();
        break;
    case FloorLayoutStrategy::PlaceAndShrink:
        break;
    }

    for (auto& r : _floor)
    {
        r.GenerateTargets(&_rng);
    }
}

#pragma region Strategy Random Placement (no overlap)

void skr::pcg::FloorGenerator::GenerateRoomsRandomPlacement()
{
    uint8_t i(0);
    while (i < _param._noRooms)
    {
        Point3d c = GenerateCenterRandomPlacement();
        float w = _rng.GetRandomFloat(_param._minWidth, _param._maxWidth);
        float h = PCG_DEFAULT_ROOM_BASE_HEIGHT;
        float l = _rng.GetRandomFloat(_param._minLength, _param._maxLength);

        Room rn(RoomType::Room, c, w, l, h);

        bool noOverlap = true;
        for (auto r : _floor)
        {
            // check if new room is inside
            if (IsOverlapping(r, rn))
            {
                noOverlap = false;
                break;
            }
        }

        if (noOverlap)
        {
            _floor.push_back(rn);
            i++;
        }
    }
}

std::array<float, 3> skr::pcg::FloorGenerator::GenerateCenterRandomPlacement(float minCoord, float maxCoord)
{
    std::array<float, 3> center;

    center[0] = _rng.GetRandomFloat(minCoord, maxCoord);
    center[1] = PCG_DEFAULT_ROOM_BASE_HEIGHT;
    center[2] = _rng.GetRandomFloat(minCoord, maxCoord);

    return center;
}

#pragma endregion

#pragma region Strategy Stacking

void skr::pcg::FloorGenerator::GenerateRoomsStacking()
{
    bool validRoomLayoutFound = false;
    while (!validRoomLayoutFound)
    {
        // initial room
        float w = _rng.GetRandomFloat(_param._minWidth, _param._maxWidth);
        float l = _rng.GetRandomFloat(_param._minLength, _param._maxLength);
        float h = PCG_DEFAULT_ROOM_HEIGHT;

        Room startRoom(RoomType::Room, Point3d{ 0.0f, 0.0f, 0.0f }, w, l, h);

        _floor.push_back(startRoom);

        std::vector<DoorDirection> tryedDirections;

        uint8_t i(1);
        while (i < _param._noRooms)
        {
            DoorDirection dir = GetRandomDirection();
            if (tryedDirections.size() == 0)
            {
                tryedDirections.push_back(dir);

                Room previousRoom = _floor.back();

                Room newRoom = PlaceNewRoom(dir, _floor.back(), PCG_DEFAULT_ROOM_STACKING_OFFSET, PCG_DEFAULT_ROOM_STACKING_OFFSET, PCG_DEFAULT_ROOM_STACKING_OFFSET);

                bool overlapFound = false;
                for (auto r : _floor)
                {
                    if (IsOverlapping(newRoom, r))
                    {
                        overlapFound = true;
                        break;
                    }
                }

                if (!overlapFound)
                {
                    _floor.back()._doors.push_back(dir);
                    newRoom._doors.push_back(InvertRoomDir(dir));
                    _floor.push_back(newRoom);

                    auto hw = PlaceHallwayBetweenTwoRooms(newRoom, dir);

                    hw._adjecentRooms.push_back(previousRoom);
                    hw._adjecentRooms.push_back(newRoom);

                    _hallways.push_back(hw);

                    tryedDirections.clear();
                    ++i;
                }
            }
            else
            {
                if (tryedDirections.size() == 4)
                {
                    // all directions tryed, no valid found -> start the whole thing again and hope for better luck
                    validRoomLayoutFound = false;
                    break;
                }

                bool freeDirFound = true;
                for (auto d : tryedDirections)
                {
                    if (dir == d)
                    {
                        freeDirFound = false;
                        break;
                    }
                }

                if (freeDirFound)
                {
                    Room previousRoom = _floor.back();
                    Room newRoom = PlaceNewRoom(dir, _floor.back(), PCG_DEFAULT_ROOM_STACKING_OFFSET, PCG_DEFAULT_ROOM_STACKING_OFFSET, PCG_DEFAULT_ROOM_STACKING_OFFSET);

                    bool overlapFound = false;
                    for (auto r : _floor)
                    {
                        if (IsOverlapping(r, newRoom))
                        {
                            overlapFound = true;
                            break;
                        }
                    }

                    if (!overlapFound)
                    {
                        _floor.back()._doors.push_back(dir);
                        newRoom._doors.push_back(InvertRoomDir(dir));
                        _floor.push_back(newRoom);

                        if (i != _param._noRooms)
                        {
                            auto hw = PlaceHallwayBetweenTwoRooms(newRoom, dir);

                            hw._adjecentRooms.push_back(previousRoom);
                            hw._adjecentRooms.push_back(newRoom);

                            _hallways.push_back(hw);
                        }

                        tryedDirections.clear();
                        ++i;
                    }
                }
            }
        }

        if (!validRoomLayoutFound)
        {
            validRoomLayoutFound = true;
        }
    }

    auto closedWalls = Room::DetermineClosedWalls(_floor[0]._doors);
    auto dir = GetRandomDirection(closedWalls);

    _startBox = PlaceSpecialRoom(_floor.front(), RoomType::Start, dir);

    closedWalls = Room::DetermineClosedWalls(_floor.back()._doors);
    dir = GetRandomDirection(closedWalls);
    _finishBox = PlaceSpecialRoom(_floor.back(), RoomType::Finish, dir);
}

skr::pcg::DoorDirection skr::pcg::FloorGenerator::GetRandomDirection()
{
    // temporary to only get one of the four straight directions, not diagonal
    return static_cast<DoorDirection>(_rng.GetRandomInteger(0, 3) * 2);
}

skr::pcg::DoorDirection skr::pcg::FloorGenerator::GetRandomDirection(std::vector<DoorDirection> dirs)
{
    // temporary to only get one of the four straight directions, not diagonal
    auto index = _rng.GetRandomInteger(0, dirs.size() - 1);
    return dirs[index];
}

skr::pcg::Room skr::pcg::FloorGenerator::PlaceNewRoom(DoorDirection dir, Room prev, float offX, float offY, float offZ)
{
    float w = _rng.GetRandomFloat(_param._minWidth, _param._maxWidth);
    float l = _rng.GetRandomFloat(_param._minLength, _param._maxLength);
    float h = PCG_DEFAULT_ROOM_HEIGHT;

    Room r(RoomType::Room, Point3d{ 0.0, 0.0, 0.0 }, w, l, h);
    float cx, cy, cz;

    switch (dir)
    {
    case DoorDirection::TOP:
        cx = prev._center[0];
        cy = PCG_DEFAULT_ROOM_BASE_HEIGHT;
        cz = prev._center[2] + prev._length * 0.5f + offZ + l * 0.5f;

        r._center = Point3d{ cx, cy, cz };
        break;

    case DoorDirection::RIGHT:
        cx = prev._center[0] + prev._width * 0.5f + offX + w * 0.5f;
        cy = PCG_DEFAULT_ROOM_BASE_HEIGHT;
        cz = prev._center[2];

        r._center = Point3d{ cx, cy, cz };
        break;

    case DoorDirection::BOTTOM:
        cx = prev._center[0];
        cy = PCG_DEFAULT_ROOM_BASE_HEIGHT;
        cz = prev._center[2] - prev._length * 0.5f - offZ - l * 0.5f;

        r._center = Point3d{ cx, cy, cz };
        break;

    case DoorDirection::LEFT:
        cx = prev._center[0] - prev._width * 0.5f - offX - w * 0.5f;
        cy = PCG_DEFAULT_ROOM_BASE_HEIGHT;
        cz = prev._center[2];

        r._center = Point3d{ cx, cy, cz };
        break;

    default:
        return r;
    }

    return r;
}

skr::pcg::Room skr::pcg::FloorGenerator::PlaceHallwayBetweenTwoRooms(Room curr, Room prev, float offX, float offY, float offZ)
{
    float cx = (curr._center[0] + prev._center[0]) * 0.5f;
    float cy = PCG_DEFAULT_ROOM_BASE_HEIGHT;
    float cz = (curr._center[2] + prev._center[2]) * 0.5f;

    Point3d center_point{ cx, cy, cz };

    float w = offX;
    float l = offZ;
    float h = PCG_DEFAULT_ROOM_HEIGHT;

    std::vector<DoorDirection> doors;
    doors.push_back(curr._doors.back());
    doors.push_back(prev._doors.back());

    Room hw(RoomType::Hallway, center_point, w, l, h);
    hw._doors = doors;
    hw._adjecentRooms.push_back(prev);
    return hw;
}

skr::pcg::Room skr::pcg::FloorGenerator::PlaceHallwayBetweenTwoRooms(Room prev, DoorDirection dir, float offX, float offY, float offZ)
{
    float cx = 0.0f, cy = 0.0f, cz = 0.0f;
    DoorDirection d = InvertRoomDir(dir);

    switch (d)
    {
    case DoorDirection::TOP:
        cx = prev._center[0];
        cy = PCG_DEFAULT_ROOM_BASE_HEIGHT;
        cz = prev._center[2] + prev._length * 0.5f + offX * 0.5f;
        break;

    case DoorDirection::RIGHT:
        cx = prev._center[0] + prev._width * 0.5f + offX * 0.5f;
        cy = PCG_DEFAULT_ROOM_BASE_HEIGHT;
        cz = prev._center[2];
        break;

    case DoorDirection::BOTTOM:
        cx = prev._center[0];
        cy = PCG_DEFAULT_ROOM_BASE_HEIGHT;
        cz = prev._center[2] - prev._length * 0.5f - offX * 0.5f;
        break;

    case DoorDirection::LEFT:
        cx = prev._center[0] - prev._width * 0.5f - offX * 0.5f;
        cy = PCG_DEFAULT_ROOM_BASE_HEIGHT;
        cz = prev._center[2];
        break;

    default:
        return Room();
    }

    Point3d center_point{ cx, cy, cz };

    float w = offX;
    float l = offZ;
    float h = PCG_DEFAULT_ROOM_HEIGHT;

    std::vector<DoorDirection> doors;
    doors.push_back(prev._doors.back());
    doors.push_back(InvertRoomDir(prev._doors.back()));

    Room hw(RoomType::Hallway, center_point, w, l, h);
    hw._doors = doors;

    return hw;
}

skr::pcg::Room skr::pcg::FloorGenerator::PlaceSpecialRoom(Room& prev, RoomType rt, DoorDirection dir)
{
    // this should not happen, as they are not "special"
    if (rt == RoomType::Room || rt == RoomType::Hallway)
        return Room();

    Point3d c{ 0.0f };

    switch (dir)
    {
    case DoorDirection::TOP: // back
    {
        c[0] = prev._center[0];
        c[1] = prev._center[1];
        c[2] = prev._center[2] + prev._length * 0.5f + PCG_DEFAULT_ROOM_STACKING_OFFSET * 0.5f;
        break;
    }

    case DoorDirection::RIGHT: // right
    {
        c[0] = prev._center[0] + prev._width * 0.5f + PCG_DEFAULT_ROOM_STACKING_OFFSET * 0.5f;
        c[1] = prev._center[1];
        c[2] = prev._center[2];
        break;
    }

    case DoorDirection::BOTTOM: // front
    {
        c[0] = prev._center[0];
        c[1] = prev._center[1];
        c[2] = prev._center[2] - prev._length * 0.5f - PCG_DEFAULT_ROOM_STACKING_OFFSET * 0.5f;
        break;
    }

    case DoorDirection::LEFT: // left
    {
        c[0] = prev._center[0] - prev._width * 0.5f - PCG_DEFAULT_ROOM_STACKING_OFFSET * 0.5f;
        c[1] = prev._center[1];
        c[2] = prev._center[2];
        break;
    }

    default:
        break;
    }

    Room r(rt, c, PCG_DEFAULT_ROOM_STACKING_OFFSET, PCG_DEFAULT_ROOM_STACKING_OFFSET, PCG_DEFAULT_ROOM_HEIGHT);
    r._doors.push_back(InvertRoomDir(dir));

    prev._doors.push_back(dir);
    r._adjecentRooms.push_back(prev);

    return r;
}

#pragma endregion

#pragma region Overlapping checks

bool skr::pcg::FloorGenerator::IsOverlapping(Room r1, Room r2)
{
    bool r1c_inside_r2 = IsInside(r2, r1._center);
    if (r1c_inside_r2)
        return true;

    bool r1llb_inside_r2 = IsInside(r2, r1.LLB());
    if (r1llb_inside_r2)
        return true;

    bool r1urb_inside_r2 = IsInside(r2, r1.URB());
    if (r1urb_inside_r2)
        return true;

    bool r1ulb_inside_r2 = IsInside(r2, r1.ULB());
    if (r1ulb_inside_r2)
        return true;

    bool r1lrb_inside_r2 = IsInside(r2, r1.LRB());
    if (r1lrb_inside_r2)
        return true;

    return false;
}

bool skr::pcg::FloorGenerator::IsInside(Room r1, Room r2)
{
    bool r_inside_r2_x1 = r1._center[0] > r2.LLB()[0];
    bool r_inside_r2_x2 = r1._center[0] < r2.URB()[0];
    bool r_inside_r2_z1 = r1._center[2] > r2.LLB()[2];
    bool r_inside_r2_z2 = r1._center[2] < r2.URB()[2];

    return r_inside_r2_x1 && r_inside_r2_x2 || r_inside_r2_z1 && r_inside_r2_z2;
}

bool skr::pcg::FloorGenerator::IsInside(Room r, Point3d p)
{
    bool p_inside_r_x1 = p[0] > r.LLB()[0];
    bool p_inside_r_x2 = p[0] < r.URB()[0];
    bool p_inside_r_z1 = p[2] > r.LLB()[2];
    bool p_inside_r_z2 = p[2] < r.URB()[2];

    return p_inside_r_x1 && p_inside_r_x2 && p_inside_r_z1 && p_inside_r_z2;
}

#pragma endregion

#pragma endregion

#pragma region FloorGeneratorParameter

skr::pcg::FloorGeneratorParameter::FloorGeneratorParameter()
    :_noRooms(10), _layoutStrategy(FloorLayoutStrategy::RandomPlacement),
    _minWidth(10.0f), _maxWidth(50.0f), _minLength(10.0f), _maxLength(50.0f), _minHeight(10.0f), _maxHeight(50.0f),
    _rng_seed(0), _rng_printSeed(false), _rng_randomSeed(true)
{
}

skr::pcg::FloorGeneratorParameter::FloorGeneratorParameter(int noRooms, FloorLayoutStrategy layoutStategy)
    : _noRooms(noRooms), _layoutStrategy(layoutStategy),
    _minWidth(10.0f), _maxWidth(50.0f), _minLength(10.0f), _maxLength(50.0f), _minHeight(10.0f), _maxHeight(50.0f),
    _rng_seed(0), _rng_printSeed(false), _rng_randomSeed(true)
{
}

void skr::pcg::FloorGeneratorParameter::SetGlobalParameter(int noRooms, FloorLayoutStrategy layoutstrategy)
{
    _noRooms = noRooms;
    _layoutStrategy = layoutstrategy;
}

void skr::pcg::FloorGeneratorParameter::SetDimensions(float minW, float maxW, float minL, float maxL, float minH, float maxH)
{
    _minWidth = minW;
    _maxWidth = maxW;

    _minLength = minL;
    _maxLength = maxL;

    _minHeight = minH;
    _maxHeight = maxH;
}

#pragma endregion