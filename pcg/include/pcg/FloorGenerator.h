#ifndef SKR_PCG_FLOORGENERATOR
#define SKR_PCG_FLOORGENERATOR

#include <vector>

#include "Room.h"
#include "rng.h"

namespace skr
{
namespace pcg
{

    //! \brief enum for used strategy to generate room layout
    enum struct FloorLayoutStrategy
    {
        RandomPlacement, //!< place rooms randomly. NOTE: unused and probably outdated. Currently does not connect rooms
        Stacking, //!< place inital room and place other adjecent to previous
        PlaceAndShrink //!< place rooms randomly and shrink them close to each other. NOTE: currently not implemented
    };

    //! \class FloorGeneratorParameter
    //! \brief holds parameters for floor generator
    struct __declspec(dllexport) FloorGeneratorParameter
    {
        uint8_t _noRooms; //!< number of rooms level consist of and are generated
        FloorLayoutStrategy _layoutStrategy; //!< used layout strategy for rooms
    
        // dimensions
        float _minWidth; //!< minimum width of a room
        float _maxWidth; //!< maximum width or a room
    
        float _minLength; //!< minimum length of a room
        float _maxLength; //!< maximum length of a room
    
        float _minHeight; //!< minimum height of a room
        float _maxHeight; //!< maximum height of a room
    
        // parameters for RNG
        bool _rng_randomSeed; //!< specifies if a random or fixed seed is used for generation
        uint32_t _rng_seed; //!< specified fixed seed. Will be ignored if random seed is used
        bool _rng_printSeed; //!< specifies if seed will be printed to console and log or not
    
        //! \brief default constructor
        FloorGeneratorParameter();
    
        //! \brief constrcutor
        //! \param noRooms number of rooms to generate
        //! \param layoutStategy layout strategy used in generation
        FloorGeneratorParameter(int noRooms, FloorLayoutStrategy layoutStategy);
    
        //! \brief sets global parameters number of rooms and layout strategy
        //! \param noRooms number of rooms to generate
        //! \param layoutstrategy layout strategy used in generation
        void SetGlobalParameter(int noRooms, FloorLayoutStrategy layoutstrategy);
    
        //! \brief set minimum and maximum dimension of rooms to be generatred
        //! \param minW minimum width
        //! \param maxW maxium width
        //! \param minL minumum length
        //! \param maxL maximum length
        //! \param minH minimum height
        //! \param maxH maximum height
        void SetDimensions(float minW, float maxW, float minL, float maxL, float minH, float maxH);
    };
    
    //! \class FloorGenerator
    //! \brief Generates a level consisting of rooms
    class __declspec(dllexport) FloorGenerator
    {
    public:
        //! \brief default constrcutor
        //! usess default parameter, does not print seed and sets a random seed
        FloorGenerator();
    
        //! \brief constructor using parameter struct
        //! uses the specified parameter struct
        //! \param param reference to parameter struct
        FloorGenerator(FloorGeneratorParameter& param);
    
    #pragma warning(disable:4251) // only using types from standard library, warning can be ignored. Source: https://docs.microsoft.com/    en-us/cpp/error-messages/compiler-warnings/compiler-warning-level-1-c4251 
        std::vector<Room> _floor; //!< list of regular rooms
        std::vector<Room> _hallways; //!< list of hallways
    #pragma warning(default:4251)
    
        Room _startBox; //!< special room for start
        Room _finishBox; //!< special room for finish
    
    public:
        //! \brief returns all room in a single list
        //! \return list of all rooms: regular, hallways and special (start and finish)
        std::vector<Room> GetAllGeometry();
    
    private:
        FloorGeneratorParameter _param; //!< parameter struct used for generation
        RNG _rng; //!< random number generator used for generation
    
        //! \brief starts generation
        void StartGeneration();
    
        //! \brief randomly places center for a room
        //! \param minCoord minimum coordinate, default 0.0f
        //! \param maxCoord maximum coordinate, default 100.0f
        //! \return coordinate
        Point3d GenerateCenterRandomPlacement(float minCoord = 0.0f, float maxCoord = 100.0f);
    
        //! \brief place room randomly
        void GenerateRoomsRandomPlacement();
    
        //! \brief checks if the two rooms are overlapping
        //! \param r first room
        //! \param r2 second room
        //! \return true if rooms overlap, otherwise false
        bool IsOverlapping(Room r, Room r2);
    
        //! \brief checks if center of second room is inside first room
        //! \param r first room
        //! \param r2 second room
        //! \return true if center of second room inside first room, otherwise false
        bool IsInside(Room r, Room r2);
    
        //! \brief checks if specified point is inside room
        //! \param r room
        //! \param p point
        //! \return true if point inside room, otherwise false
        bool IsInside(Room r, Point3d p);
    
        //! \brief generate a room layout using the stacking-strategy
        void GenerateRoomsStacking();
    
        //! \brief gets a random direction for a door
        //! \return random DoorDirection
        DoorDirection GetRandomDirection();
    
        //! \brief gets a random direction from a list of directions
        //! \param dirs list of DoorDirections to choose from
        //! \return random DoorDirection
        DoorDirection GetRandomDirection(std::vector<DoorDirection> dirs);
    
        //! \\brief places a new room
        //! \param dir direction in which new room is placed
        //! \param prev previous room to place new room off
        //! \param offX offset in x-axis, default PCG_DEFAULT_ROOM_STACKING_OFFSET
        //! \param offY offset in y-axis, default PCG_DEFAULT_ROOM_STACKING_OFFSET
        //! \param offZ offset in z-axis, default PCG_DEFAULT_ROOM_STACKING_OFFSET
        //! \return new room
        Room PlaceNewRoom(DoorDirection dir, Room prev, 
            float offX = PCG_DEFAULT_ROOM_STACKING_OFFSET, float offY = PCG_DEFAULT_ROOM_STACKING_OFFSET, float offZ =  PCG_DEFAULT_ROOM_STACKING_OFFSET
        );
    
        //! \brief place hallway in the middle of two rooms
        //! only works with quadratic rooms
        //! \param curr current room
        //! \param prev previous room
        //! \param offX offset in x-axis, default PCG_DEFAULT_ROOM_STACKING_OFFSET
        //! \param offY offset in y-axis, default PCG_DEFAULT_ROOM_STACKING_OFFSET
        //! \param offZ offset in z-axis, default PCG_DEFAULT_ROOM_STACKING_OFFSET
        //! \return new room
        Room PlaceHallwayBetweenTwoRooms(Room curr, Room prev, 
            float offX = PCG_DEFAULT_ROOM_STACKING_OFFSET, float offY = PCG_DEFAULT_ROOM_STACKING_OFFSET, float offZ =  PCG_DEFAULT_ROOM_STACKING_OFFSET
        );
    
        //! \brief place hallway between two rooms
        //! uses size and direction to place between rectangular rooms correctly
        //! \param prev previous room
        //! \param dir direction hallways is placed in relation to previous room
        //! \param offX offset in x-axis, default PCG_DEFAULT_ROOM_STACKING_OFFSET
        //! \param offY offset in y-axis, default PCG_DEFAULT_ROOM_STACKING_OFFSET
        //! \param offZ offset in z-axis, default PCG_DEFAULT_ROOM_STACKING_OFFSET
        //! \return new room
        Room PlaceHallwayBetweenTwoRooms(Room prev, DoorDirection dir,
            float offX = PCG_DEFAULT_ROOM_STACKING_OFFSET, float offY = PCG_DEFAULT_ROOM_STACKING_OFFSET, float offZ =  PCG_DEFAULT_ROOM_STACKING_OFFSET
        );
    
        //! \brief place special room
        //! \param prev reference to previous room
        //! \param rt type of new special room
        //! \param dir direction special room is placed in relation to previous room
        //! \return special room
        Room PlaceSpecialRoom(Room& prev, RoomType rt, DoorDirection dir);
    };

}
}

#endif //! SKR_PCG_FLOORGENERATOR