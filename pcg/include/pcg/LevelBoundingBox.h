#ifndef SKR_PCG_LEVEL_BOUNDING_BOX
#define SKR_PCG_LEVEL_BOUNDING_BOX

#include <array>
#include "Definitions.h"
#include "Room.h"

namespace skr
{
namespace pcg
{
    //! \class LevelBoundingBox
    //! \brief defines the overall extends of a level in 2d.
    /*! On a plane in x- and z-axis, as y is considered the height. Height is currently not used, as all rooms of a level are on one plane. */
    class __declspec(dllexport) LevelBoundingBox
    {
    public:
        Point3d _center; //!< center point
        Point3d _LLB; //!< coordinate of LowerLeftBottom
        Point3d _LRB; //!< coordinate of LowerRightBottom
        Point3d _ULB; //!< coordinate of UpperLeftBottom
        Point3d _URB; //!< coordinate of UpprRightBottom

        //! \brief default constructor
        //! sets all coordinates to (0,0,0)
        LevelBoundingBox();

        //! \brief constructor
        //! \param center center point
        //! \param width exted in x-axis
        //! \param length extend in z-axis
        LevelBoundingBox(Point3d center, float width, float length);

        //! \brief constructor
        //! calculates bounding box given a single room
        //! \param room room level consists of
        LevelBoundingBox(skr::pcg::Room room);

        //! \brief constructor
        //! calcualtes overall bounding box of a list of rooms
        //! \param rooms list of rooms the level consists of
        LevelBoundingBox(std::vector<skr::pcg::Room> rooms);
    };

}
}

#endif //! SKR_PCG_LEVEL_BOUNDING_BOX
