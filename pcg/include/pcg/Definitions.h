#ifndef PCG_DEFINITIONS
#define PCG_DEFINITIONS

#include <array>
#include <vector>
#include <string>

namespace skr
{
    namespace pcg
    {

#pragma region Typdefs

        typedef std::array<float, 2> Point2d; //!< 2d point
        typedef Point2d Vec2; //!c 2d vector

        typedef std::array<float, 3> Point3d; //!< 3d point
        typedef Point3d Vec3; //!c 3d vector

#pragma endregion

#pragma region data structures

        //! enum for OpenGL Texture Wrap Parameters
        enum struct OpenGLTextureWrapParameter
        {
            OGL_REPEAT = 0x2901,
            OGL_MIRRORED_REPEAT = 0x8370,
            OGL_CLAMP_TO_EDGE = 0x812F,
            OGL_CLAMP_TO_BORDER = 0x812D
        };

        //! struct describing a vertex
        struct Vertex
        {
            Vec3 Position; //! position of vertex
            Vec3 Normal; //! normal at vertex
            Vec2 TexCoords; //! texture coordinates at vertex
        };

        //! strcut describing a face
        struct Face
        {
            std::vector<Vertex> vertices; //! vertices of face
            std::vector<uint32_t> indices; //! indices of vertices. If index vertices aren't used, this i empty
            Vec3 color; //! color to draw the face in if no texture is specified
            std::string diffuseTexturePath; //! path to diffuse texture. empty if no texture is used
            std::string normalTexturePath; //! path to normal texture. empty if no texture is used
            OpenGLTextureWrapParameter textureWrapParameter; //! OpenGL texture wrap parameter for this face
        };

#pragma endregion

#pragma region Constant Values

#define MIN_TARGETS 2
#define TARGETS_PER_SQUARE_SIZE 200

        constexpr float PCG_DEFAULT_ROOM_HEIGHT = 5.0f; //!< default value for height of a room
        constexpr float PCG_DEFAULT_ROOM_BASE_HEIGHT = 0.0f; //!< default value for base eight of the level
        constexpr float PCG_DEFAULT_ROOM_STACKING_OFFSET = 3.0f; //!< default space between rooms; also size of hallways

        constexpr float PCG_DEFAULT_TARGET_SIZE = 2.0f; //!< default size of targets; used to prevent overlap
        constexpr float PCG_DEFAULT_TARGET_MOVEMENT_MAX_EXTEND = PCG_DEFAULT_TARGET_SIZE * 1.5f; //!< default extend to which a target can move at maxmium
        constexpr float PCG_DEFAULT_TARGET_MOVEMENT_MAX_EXTEND_VERTICAL = (PCG_DEFAULT_ROOM_HEIGHT - PCG_DEFAULT_TARGET_SIZE) * 0.5f; //!< default extend to which a target can move vertically (y-axis) at maximum
        constexpr float PCG_DEFAULT_TARGET_MAX_MOVEMENT_SPEED = 2.0f; // default maximum movement speed of a target


        // Default Values for normals of faces of a room
        constexpr Vec3 DefaultNormal_Bottom { 0.0f, 1.0f, 0.0f };   //!> default normal for bottom face
        constexpr Vec3 DefaultNormal_Top { 0.0f, -1.0f, 0.0f };     //!> default normal for top face

        constexpr Vec3 DefaultNormal_Back{ 0.0f, 0.0f, -1.0f };     //!> default normal for back face
        constexpr Vec3 DefaultNormal_Front { 0.0f, 0.0f, 1.0f };    //!> default normal for front face

        constexpr Vec3 DefaultNormal_Right { -1.0f, 0.0f, 0.0f };   //!> default normal for right face
        constexpr Vec3 DefaultNormal_Left { 1.0f, 0.0f, 0.0f };     //!> default normal for left face
#pragma endregion

    }
}

#endif //!PCG_DEFINITIONS
