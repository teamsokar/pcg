#include <iostream>
#include <vector>
#include <string>

#include <fstream>

#include "pcg\Room.h"
#include "pcg\FloorGenerator.h"

//! \brief Demo application for pcg lib
//! outputs list of generated rooms to cmd
int main()
{
	using namespace skr::pcg;

	uint8_t num_rooms(10);

    FloorGeneratorParameter params(num_rooms, FloorLayoutStrategy::Stacking);
    params.SetDimensions(20.0f, 50.0f, 20.0f, 50.0f, 5.0f, 5.0f);
    //params.SetSeed(1749);
    params._rng_printSeed = true;

    FloorGenerator generator(params);    

    std::vector<Room> geometry = generator.GetAllGeometry();

	std::cout << "Rooms:" << std::endl;
	for (auto r : geometry)
	{
        std::cout << r.PrintRoomMeasurements(true) << std::endl;
	}
}
