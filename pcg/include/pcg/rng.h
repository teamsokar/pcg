#ifndef SKR_PCG_RNG_H
#define SKR_PCG_RNG_H

#include "..\..\pch.h"

#include <random>

namespace skr
{
namespace pcg
{
    //! \class RNG
    //! \brief random number generator helper class
    class __declspec(dllexport) RNG
    {
    public:
        //! \brief constructor
        //! uses random seed
        //! \param printSeed set true to print seed to cmd and log, otherweise false. Default: false
        RNG(bool printSeed = false);
    
        //! \brief Initializes random number generator with random seed
        void SetRandomSeed();

        //! \brief Initializes random number generator with given fixed seed
        //! \param seed given seed
        void SetSeed(uint32_t seed);

        //! \brief Set printSeed Parameter
        //! \param printSeed true to print seed to cmd and log, otherwise false
        void SetPrintSeed(bool printSeed);
    
        //! \brief returns a random interger from given range
        //! \param lower lower bound
        //! \param upper upper bound
        //! \return random integer
        int GetRandomInteger(const int lower, const int upper);

        //! \brief returns a random float from given range
        //! \param lower lower bound
        //! \param upper upper bound
        //! \return random float
        float GetRandomFloat(const float lower, const float upper);

        //! \brief returns a random double from given range
        //! \param lower lower bound
        //! \param upper upper bound
        //! \return random idouble
        double GetRandomDouble(const double lower, const double upper);
    
        uint32_t _seed; //!< Seed value for random number generator // HACK this only public to make the core application update it if an invalid one is found. I really don't like it, but its the best I can do for now
    
    private:
#pragma warning(disable:4251) // only using types from standard library, warning can be ignored. Source: https://docs.microsoft.com/en-s/cpp/   error-messages/compiler-warnings/compiler-warning-level-1-c4251 
        std::mt19937 _randomEngine; //!c mersenne-twister engine for random number generator
        std::vector<uint32_t> _seedBlacklist = { 72304668, 2126917180, 3620160884, 429540020 }; //!c list of seeds know to not produce a viable level, instead puts generation into an endless loop
#pragma warning(default:4251)
    
    
        bool _printSeed; //!c true to print seed to cmd and log, otherwise false
    
        //! \brief Initializes random number generator with random seed
        void InitWithRandomSeed();

        //! \brief Initializes random number generator with given fixed seed
        //! \param seed given seed
        void InitWithFixedSeed(uint32_t seed);
    
        //! \brief check if a given seed is not on the blacklist
        //! this is easy solution/hack to prevent levels with overlapping rooms, etc. to be generated
        //! \param seed seed value to check
        //! \return true if seed is not on blacklist, otherwise false
        bool CheckViableSeed(uint32_t seed);

        //! \brief Generates a new seed that is checked to be viable
        //! \return seed value that is not on blacklist
        uint32_t GenerateViableSeed();
    };
}
}


#endif // ! SKR_PCG_RNG_H